FROM savonet/liquidsoap:v2.1.2

ARG UID=1001
ARG GID=1001

WORKDIR /wrek

COPY --chown=liquidsoap:liquidsoap ./scripts .

USER root
RUN usermod -u $UID liquidsoap
RUN groupmod -g $GID liquidsoap
RUN mkdir -p /wrek/archive
RUN chown -R liquidsoap:liquidsoap /wrek

USER liquidsoap

ENTRYPOINT ["/usr/bin/tini", "--", "/usr/bin/liquidsoap"]
