set -e

# cd into the directory of the file to rename
cd "${1%/*}"

# this is the file to rename. expected format is "DAY0000_tmp.mp3"
tmp_filename=$(basename "$1")

# check filename format
if [[ $tmp_filename != *"_tmp"* ]]; then
  echo "Unexpected filename format. Missing \"_tmp\""
  exit 1
fi

# check if the file exists
if [ ! -f "$tmp_filename" ]; then
  echo "File $1 does not exist"
  exit 1
fi

# filename without any suffix. example "DAY0000.mp3"
plain_filename=$(echo $tmp_filename | sed 's/_tmp//g')

# filename with the _old suffix. example "DAY0000_old.mp3"
old_filename=$(echo $tmp_filename | sed 's/_tmp/_old/g')

echo "SCRIPT_DEBUG:: renaming"

# rename the current DAY0000.mp3 to DAY0000_old.mp3
if [ -f "$plain_filename" ]; then
    mv "$plain_filename" "$old_filename"
fi

# rename the tmp file to DAY0000.mp3
mv "$tmp_filename" "$plain_filename"
