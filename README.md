# WREK Archiver

## Setup
1. Make a copy of the example env file: `cp example.env .env`
1. Replace the values in .env
1. Build the archiver image. If deploying, run `./build.sh` so that the liquidsoap user has the UID and GID set
correctly. Otherwise, if you're using Docker Desktop, you can just run `docker-compose build`. You may also have
to chown the `./data` directory, i.e. `sudo chown -R USERNAME:USERGROUP ./data` if `root` owns it
1. Run `docker-compose up -d` to start the container

## Updating
- Run `docker-compose pull` and then `docker-compose up -d` to update and re-create the container
- To rebuild the liquidsoap container, use `./build.sh` or `docker-compose build` (see above). Otherwise, to update,
manually bump the version number in the Dockerfile

